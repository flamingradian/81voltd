/* SPDX-License-Identifier: GPL-2.0-or-later */
/*
 * Copyright (c) 2024, Richard Acayan. All rights reserved.
 */

#ifndef QVD_MODEM_H
#define QVD_MODEM_H

#include <gio/gio.h>
#include <libmm-glib.h>
#include <stdint.h>

#include "qvd-connection.h"

G_BEGIN_DECLS

#define QVD_TYPE_MODEM qvd_modem_get_type()
G_DECLARE_FINAL_TYPE(QVDModem, qvd_modem, QVD, MODEM, GObject)

QVDModem *qvd_modem_new(GError **err);

QVDConnection *qvd_modem_get_connection(QVDModem *ctx,
					const gchar *apn,
					MMBearerIpFamily ip_family);

G_END_DECLS

#endif /* QVD_MODEM_H */
