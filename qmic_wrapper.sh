#!/bin/sh
# SPDX-License-Identifier: GPL-2.0-or-later
#
# QMI interface compiler wrapper from sensh
#
# Copyright (C) 2023 Richard Acayan
#

qmic="$1"

#
# Usage: qmic_wrapper.sh QMIC INPUT OUTPUT_DIRECTORY
#
# Example: qmic_wrapper.sh /usr/bin/qmic ../qmi/sns_client.qmi qmi/
#

#
# We need to move into the output directory so QMIC puts the files in the
# correct place, but the path to the input might be relative to the directory
# we are already in. Open the file now and pipe it to stdin so the pathname
# gets processed relative to the correct directory.
#
exec < "$2"

cd "$3"
exec "$qmic" -k
