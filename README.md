**Moved to https://gitlab.postmarketos.org/modem/81voltd**

This is a simple implementation of the IMS Data service on QMI/QRTR. It handles
the known and important requests to start and stop a connection through the
ModemManager D-Bus API, treating other requests as no operation.

Some known issues remain:

- Stopping the implementation does not release or disconnect any bearers. This
  makes it hard to debug, requiring the modem be set to low-power, then back
  online.
- ModemManager errors are not communicated to the modem.
