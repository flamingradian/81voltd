// SPDX-License-Identifier: GPL-2.0-or-later
/*
 * Copyright (c) 2024, Richard Acayan. All rights reserved.
 */

#include <glib.h>

#include "qvd-modem.h"
#include "qvd-server.h"

int main(void)
{
	GMainLoop *loop;
	GError *err = NULL;
	QVDModem *modem;
	QVDServer *server;

	modem = qvd_modem_new(&err);
	if (err != NULL) {
		g_printerr("Failed to create connection manager: %s\n", err->message);
		g_error_free(err);
		return 1;
	}

	server = qvd_server_new(modem, &err);
	if (err != NULL) {
		g_printerr("Failed to create IMSD server: %s\n", err->message);
		g_error_free(err);
		return 1;
	}

	loop = g_main_loop_new(NULL, FALSE);
	g_main_loop_run(loop);

	g_object_unref(server);
	g_object_unref(modem);

	return 0;
}
