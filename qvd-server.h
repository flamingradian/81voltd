/* SPDX-License-Identifier: GPL-2.0-or-later */
/*
 * Copyright (c) 2024, Richard Acayan. All rights reserved.
 */

#ifndef QVD_SERVER_H
#define QVD_SERVER_H

#include <gio/gio.h>
#include <libmm-glib.h>

#include "qvd-modem.h"

G_BEGIN_DECLS

#define QVD_TYPE_SERVER qvd_server_get_type()
G_DECLARE_FINAL_TYPE(QVDServer, qvd_server, QVD, SERVER, GObject)

QVDServer *qvd_server_new(QVDModem *modem, GError **err);

void qvd_server_set_modem(QVDServer *server, QVDModem *modem);

G_END_DECLS

#endif /* QVD_SERVER_H */
