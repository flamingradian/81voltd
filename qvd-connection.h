/* SPDX-License-Identifier: GPL-2.0-or-later */
/*
 * Copyright (c) 2024, Richard Acayan. All rights reserved.
 */

#ifndef QVD_CONNECTION_H
#define QVD_CONNECTION_H

#include <gio/gio.h>
#include <glib.h>
#include <libmm-glib.h>

G_BEGIN_DECLS

#define QVD_TYPE_CONNECTION qvd_connection_get_type()
G_DECLARE_FINAL_TYPE(QVDConnection, qvd_connection, QVD, CONNECTION, GObject)

QVDConnection *qvd_connection_new(MMBearerIpFamily ip_type);

void qvd_connection_get_handles(const QVDConnection *conn,
				guint32 *sub_id, guint32 *conn_id,
				guint8 *local_id);
void qvd_connection_set_handles(QVDConnection *conn,
				guint32 sub_id, guint32 conn_id,
				guint8 local_id);

void qvd_connection_connect(QVDConnection *conn);
void qvd_connection_disconnect(QVDConnection *conn);
void qvd_connection_set_bearer(QVDConnection *conn, MMBearer *bearer);

G_END_DECLS

#endif /* QVD_CONNECTION_H */
